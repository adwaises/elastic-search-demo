package ru.kaa.elasticsearchdemo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.IndexOperations;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kaa.elasticsearchdemo.model.Book;
import ru.kaa.elasticsearchdemo.service.BookService;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ElasticSearchDemoApplication.class)
public class ElasticSearchDemoApplicationTests {

    @Autowired
    private BookService bookService;

    @Autowired
    private ElasticsearchOperations operations;

    @Before
    public void before() {
        IndexOperations indexOps = operations.indexOps(Book.class);
        indexOps.delete();
        indexOps.create();
        indexOps.putMapping();
        indexOps.refresh();
    }

    @Test
    public void testSave() {
        Book book = new Book("1001", "Elasticsearch Basics", "Rambabu Posa", "Content is this", "23-FEB-2017");
        Book testBook = bookService.save(book);

        assertNotNull(testBook.getId());
        assertEquals(testBook.getTitle(), book.getTitle());
        assertEquals(testBook.getAuthor(), book.getAuthor());
        assertEquals(testBook.getReleaseDate(), book.getReleaseDate());
    }

    @Test
    public void testFindOne() {
        Book book = new Book("1001", "Elasticsearch Basics", "Rambabu Posa", "No content", "23-FEB-2017");
        bookService.save(book);

        Book testBook = bookService.findOne(book.getId());

        assertNotNull(testBook.getId());
        assertEquals(testBook.getTitle(), book.getTitle());
        assertEquals(testBook.getAuthor(), book.getAuthor());
        assertEquals(testBook.getReleaseDate(), book.getReleaseDate());
    }

    @Test
    public void testFindByTitle() {
        Book book = new Book("1001", "Elasticsearch Basics", "Rambabu Posa", "Content is this", "23-FEB-2017");
        bookService.save(book);

        List<Book> byTitle = bookService.findByTitle(book.getTitle(), PageRequest.of(0, 10));
        assertThat(byTitle.size(), is(1));
    }

    @Test
    public void testFindByAuthor() {
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("1001", "Elasticsearch Basics", "Rambabu Posa", "Content is this", "23-FEB-2017"));
        bookList.add(new Book("1002", "Apache Lucene Basics", "Rambabu Posa", "Content is this", "13-MAR-2017"));
        bookList.add(new Book("1003", "Apache Solr Basics", "Rambabu Posa", "Content is this", "21-MAR-2017"));
        bookList.add(new Book("1007", "Spring Data + ElasticSearch", "Rambabu Posa", "Content is this", "01-APR-2017"));
        bookList.add(new Book("1008", "Spring Boot + MongoDB", "Mkyong", "Content is this", "25-FEB-2017"));

        for (Book book : bookList) {
            bookService.save(book);
        }

        Page<Book> byAuthor = bookService.findByAuthor("Rambabu Posa", PageRequest.of(0, 10));
        assertThat(byAuthor.getTotalElements(), is(4L));

        Page<Book> byAuthor2 = bookService.findByAuthor("Mkyong", PageRequest.of(0, 10));
        assertThat(byAuthor2.getTotalElements(), is(1L));
    }

    @Test
    public void testDelete() {

        Book book = new Book("1001", "Elasticsearch Basics", "Rambabu Posa", "Content is this", "23-FEB-2017");
        bookService.save(book);
        bookService.delete(book);
        Book testBook = bookService.findOne(book.getId());
        assertNull(testBook);
    }

    @Test
    public void testCriteriaSearch() {
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("1001", "Elasticsearch Basics", "Rambabu Posa", "Content is this", "23-FEB-2017"));
        bookList.add(new Book("1002", "Apache Lucene Basics", "Rambabu Posa", "Content is this", "13-MAR-2017"));
        bookList.add(new Book("1003", "Apache Solr Basics", "Rambabu Posa", "Content is this", "21-MAR-2017"));
        bookList.add(new Book("1007", "Spring Data + ElasticSearch", "Rambabu Posa", "Content is this", "01-APR-2017"));
        bookList.add(new Book("1008", "Spring Boot + MongoDB", "Mkyong", "Content is this", "25-FEB-2017"));

        for (Book book : bookList) {
            bookService.save(book);
        }

        List<Book> byAuthor = bookService.findUseCriteriaQuery("Rambabu Posa", PageRequest.of(0, 10));
        assertThat(byAuthor.size(), is(4));
    }

    @Test
    public void testNativeCriteriaSearch() {
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("1001", "Elasticsearch Basics", "Rambabu Posa", "Content is this", "23-FEB-2017"));
        bookList.add(new Book("1002", "Apache Lucene Basics", "Rambabu Posa", "Content is this", "13-MAR-2017"));
        bookList.add(new Book("1003", "Apache Solr Basics", "Rambabu Posa", "Content is this", "21-MAR-2017"));
        bookList.add(new Book("1007", "Spring Data + ElasticSearch", "Rambabu Posa", "Content is this", "01-APR-2017"));
        bookList.add(new Book("1008", "Spring Boot + MongoDB", "Mkyong", "Content is this", "25-FEB-2017"));

        for (Book book : bookList) {
            bookService.save(book);
        }

        List<Book> byAuthor = bookService.findUseNativeSearchQueryBuilder("Spring", PageRequest.of(0, 10));
        assertThat(byAuthor.size(), is(2));
    }


    @Test
    public void testMustSearch() {
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("1001", "Elasticsearch Basics", "Rambabu Posa", "Content is this", "23-FEB-2017"));
        bookList.add(new Book("1002", "Apache Lucene Basics", "Rambabu Posa", "Content is this", "13-MAR-2017"));
        bookList.add(new Book("1003", "Apache Solr Basics", "Rambabu Posa", "Content is this", "21-MAR-2017"));
        bookList.add(new Book("1007", "Spring Data + ElasticSearch", "Rambabu Posa", "Content is this", "01-APR-2017"));
        bookList.add(new Book("1008", "Spring Boot + MongoDB", "Mkyong", "Content is this", "25-FEB-2017"));

        for (Book book : bookList) {
            bookService.save(book);
        }

        List<Book> byAuthor = bookService.findUseBoolNativeSearchQueryBuilder("Spring", "Rambabu Posa", PageRequest.of(0, 10));
        assertThat(byAuthor.size(), is(2));
    }

    @Test
    public void testQueryInRepository() {
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("1001", "Elasticsearch Basics", "Rambabu Posa", "Content is this", "23-FEB-2017"));
        bookList.add(new Book("1002", "Apache Lucene Basics", "Rambabu Posa", "Content is this", "13-MAR-2017"));
        bookList.add(new Book("1003", "Apache Solr Basics", "Rambabu Posa", "Content is not", "21-MAR-2017"));
        bookList.add(new Book("1007", "Spring Data + ElasticSearch", "Rambabu Posa", "Content is this", "01-APR-2017"));
        bookList.add(new Book("1008", "Spring Boot + MongoDB", "Mkyong", "Content is not", "25-FEB-2017"));

        for (Book book : bookList) {
            bookService.save(book);
        }

        List<Book> byAuthor = bookService.findUseNativeSearchQueryInRepository("Content this", PageRequest.of(0, 10));
        assertThat(byAuthor.size(), is(3));
    }

}

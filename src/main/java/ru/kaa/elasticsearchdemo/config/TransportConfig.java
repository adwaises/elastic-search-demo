package ru.kaa.elasticsearchdemo.config;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.elasticsearch.core.AbstractElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import java.net.InetAddress;


@Configuration
@Profile("transport")

public class TransportConfig {
    @Value("${elasticsearch.host}")
    private String esHost;

    @Value("${elasticsearch.transport.port}")
    private int esTransportPort;

    @Value("${elasticsearch.clustername}")
    private String esClusterName;

    @Bean
    public Client client() throws Exception {
        Settings esSettings = Settings.builder()
                .put("cluster.name", esClusterName)
                .build();

        TransportClient transportClient = new PreBuiltTransportClient(esSettings);
        transportClient.addTransportAddress(new TransportAddress(InetAddress.getByName(esHost), esTransportPort));
        return transportClient;
    }

    @Bean
    public AbstractElasticsearchTemplate elasticsearchTemplate() throws Exception {
        return new ElasticsearchTemplate(client());
    }
}

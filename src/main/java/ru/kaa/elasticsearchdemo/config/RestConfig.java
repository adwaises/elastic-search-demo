package ru.kaa.elasticsearchdemo.config;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.core.AbstractElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;

import java.net.InetSocketAddress;

@Configuration
@Profile("rest")
public class RestConfig {

    @Value("${elasticsearch.host}")
    private String esHost;

    @Value("${elasticsearch.http.port}")
    private int esHttpPort;

    @Bean
    public RestHighLevelClient elasticsearchClient() {
        final ClientConfiguration configuration = ClientConfiguration.create(
                InetSocketAddress.createUnresolved(esHost, esHttpPort)
        );
        return RestClients.create(configuration).rest();
    }

    @Bean
    public AbstractElasticsearchTemplate elasticsearchTemplate() {
        return new ElasticsearchRestTemplate(elasticsearchClient());
    }

}